#POS Vibration Testing
#Written by Benjamin Stadnik
#Orbital Research Ltd.
#2021-08-19

import pyvisa as visa
import csv
import sys
import time
import math
import json
import os
import shutil
import datetime
import serial

PORT = 'COM6'
PERIOD = 10 #minutes
HEADER = ['Time','Output1','Current1','Output2','Current2']

PM1_VISA = 'USB0::0x0957::0x2E18::MY51140035::0::INSTR'
PM2_VISA = 'USB0::0x0957::0x2E18::MY48100286::0::INSTR'
AMM1_VISA = 'GPIB1::22::INSTR'
AMM2_VISA = 'GPIB1::25::INSTR'

def COM_Init(COM_port):  
    #Begin serial connection
    global ser
    ser = serial.Serial(
        port=str(COM_port),
        baudrate=9600,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS
    )

def timestamp():
    now = {}
    current_time = datetime.datetime.now()
    now[0] = current_time.strftime("%Y %b %d %H:%M:%S")
    now[1] = current_time.strftime("%Y%b%d_%H%M%S")
    return now

def CSV_Init(filename, header):
    if filename == '':
        filename = 'No_Name'
    name = input('Tester name:')
    SN = input('Unit serial number:')
    global workbook, FILENAME
    current_time = timestamp()
    FILENAME = filename + '_' + current_time[1] + '.csv'
    with open(FILENAME, 'w', newline='') as f:
        workbook = csv.writer(f)
        workbook.writerow(['Tester:', name])
        workbook.writerow(['Unit:', SN])
        workbook.writerow(['Date:', current_time[0]])
        workbook.writerow([])
        workbook.writerow(HEADER)

def main():
    #----Initiazlizations----
    print('Connecting to resources...')
    COM_Init(PORT)
    powerMeterFreq = 10000000    
    rm = visa.ResourceManager()
    PM1 = rm.open_resource(PM1_VISA)
    PM1.timeout = 10000
    PM2 = rm.open_resource(PM2_VISA)
    PM2.timeout = 10000
    AMM1 = rm.open_resource(AMM1_VISA)
    AMM1.timeout = 10000
    AMM2 = rm.open_resource(AMM2_VISA)
    AMM2.timeout = 10000
    
    PM1.write(':INITiate1:CONTinuous %d' % (1)) 
    PM1.write(':SENSe:FREQuency:FIXed %G' % (powerMeterFreq))
    PM2.write(':INITiate1:CONTinuous %d' % (1))
    PM2.write(':SENSe:FREQuency:FIXed %G' % (powerMeterFreq))
    print('Connected to resources.')

    #----Main Function begin-----
    filename = input('Enter filename:')
    CSV_Init(filename, HEADER)
    prev_time = 0
    input('Press Enter to begin test')
    print(HEADER)
    while(1):       
        timer = time.time()
        if((timer - prev_time) > (PERIOD * 60)):
            current_time = timestamp()[0]
            #print(current_time)
            prev_time = timer
            
            #----------------Do function-------------------
            ser.write(b'SOUT0\r')
            time.sleep(5)
            Output1 = float(PM1.query(':FETCh1?'))
            Output2 = float(PM2.query(':FETCh1?'))
            Current1 = float(AMM1.query(':FETCh1?'))
            Current2 = float(AMM2.query(':FETCh1?'))
            time.sleep(5)
            ser.write(b'SOUT1\r')
            print([current_time, Output1, Current1, Output2, Current2])
            with open(FILENAME, 'a', newline='') as f:
                workbook = csv.writer(f)
                workbook.writerow([current_time, Output1, Current1, Output2, Current2])
            #----------------------------------------------
try:
    
    main()
except KeyboardInterrupt:
    print('Keyboard interrupt detected. Stopping program')
    exit()

#Commands:
#Voltage setpoint: b'VOLTXXX\r' ; XXX=234=23.4V
#Current setpoint: b'CURRXXX\r' ; XXX=241=24.1A
#Output control: b'SOUTX\r'     ; X=0=ON, X=1=OFF
